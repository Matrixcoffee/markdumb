import html
import re


def html_escape(text, quote=False):
	return html.escape(text, quote)


def pass1(markdown):
	""" Pass 1: For security reasons, this implementation does
	    not allow HTML from the source text. Escape it all. """
	return html_escape(markdown)


def pass2(data):
	""" Pass 2: Convert ATX headlines """
	body = re.split(r"^(#+) +(.*?)(?:\n|$)", data, flags=re.MULTILINE)

	#for i, b in enumerate(body):
	#	print("Body({}): {!r}".format(i, b))

	for i, b in enumerate(body):
		if i % 3 == 0:
			for l in b.splitlines(keepends=True):
				yield l
		if i % 3 == 1:
			hlevel = len(b)
		if i % 3 == 2:
			yield "<h{0}>{1}</h{0}>\n".format(hlevel, b)


def pass3(iterable):
	""" Pass 3: Unordered lists """
	inlist = False
	blankline_debt = False
	for b in iterable:
		if b.startswith("* "):
			if not inlist:
				yield "<ul>\n"
			inlist = True
			yield "  <li>{}</li>\n".format(b[2:].strip())
			blankline_debt = False
		else:
			if inlist and b.strip() == "":
				blankline_debt = True
			else:
				if inlist:
					yield "</ul>\n"
					inlist = False
				if blankline_debt:
					yield "\n"
					blankline_debt = False
				yield b


def pass4(iterable):
	""" Pass 4: Code spans """
	re_code = re.compile(r"(^|[^`])(`+)([^`]|[^`].*?[^`])\2([^`]|$)")

	for b in iterable:
		s = re_code.split(b)
		for i, chunk in enumerate(s):
			if i % 5 == 0: # "outside" content
				yield chunk
			if i % 5 == 1: # Last character of outside content
				yield chunk
			if i % 5 == 3: # "inside" content
				yield "<code>"
				# Here's a stupid trick: Split the code block into
				# individual characters so the passes after this can't
				# "see" any patterns. This works because we keep the
				# newlines, and after everything is nicely spliced back
				# together, there's no evidence this ever happened.

				for c in chunk:
					yield c
				yield "</code>"
			if i % 5 == 4: # First character of outside content
				yield chunk


def pass5(iterable):
	""" Pass 5: Formatting (images, links, strong and emphasis) """
	re_image = re.compile(r"""!\[(.*?)\]\((.+?)(?:\s+(["'])(.*?)\3)?\)""")
	re_link = re.compile(r"""\[(.*?)\]\((.+?)(?:\s+(["'])(.*?)\3)?\)""")
	re_strong1 = re.compile(r"()\*\*(\S.*?\S|\S)\*\*()")
	re_strong2 = re.compile(r"()__(\S.*?\S|\S)__()")
	re_em1 = re.compile(r"()\*([^\s*].*?[^\s*]|[^\s*])\*()")
	re_em2 = re.compile(r"(\s|^)_([^\s_].*?[^\s_]|[^\s_])_(\s|$)")

	h_image = r"""<img src="\2" alt="\1" title="\3">"""
	h_strong = r"\1<strong>\2</strong>\3"
	h_em = r"\1<em>\2</em>\3"

	def h_link(match):
		txt = match.group(1)
		url = match.group(2)
		title = match.group(3)

		if txt == "": txt = url
		url = html_escape(url)
		txt = html_escape(txt)
		if title: h_title = " title=\"{}\"".format(html_escape(title, quote=True))
		else: h_title = ""

		return "<a href=\"{}\"{}>{}</a>".format(url, h_title, txt)

	replacers = (
		(re_image, h_image),
		(re_link, h_link),
		(re_strong1, h_strong),
		(re_strong2, h_strong),
		(re_em1, h_em),
		(re_em2, h_em)
	)

	for b in iterable:
		for pattern, replacement in replacers:
			b = pattern.sub(replacement, b)

		yield b


def as_html(markdown):
	x = pass1(markdown)
	x = pass2(x)
	x = pass3(x)
	x = pass4(x)
	x = pass5(x)

	return "".join(x)


if __name__ == '__main__':
	import sys
	with open(sys.argv[1]) as f: markdown = f.read()
	html = as_html(markdown)
	print(html)
